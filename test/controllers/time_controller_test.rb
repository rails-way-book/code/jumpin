require 'test_helper'

class TimeControllerTest < ActionDispatch::IntegrationTest
  test "should get now" do
    get time_now_url
    assert_response :success
  end

end
